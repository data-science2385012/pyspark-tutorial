The below document specifies how conda and jupyter should be setup. This is how this repo is also setup. 

https://towardsdatascience.com/how-to-set-up-anaconda-and-jupyter-notebook-the-right-way-de3b7623ea4a

Once the initial installations are done, it is recommended to kill all the open terminals (by clicking on the dustbin icon and not on the x icon) and start fresh terminals to carry out any activities.

To start jupyter lab, run `jupyter lab --NotebookApp.allow_origin="*" ` from the base environent only. The extra argument is to solve the CORS issue originated from the notebook connecting to gitpod.

Python spark shell can be run by typing `pyspark` in the terminal of **Gitpod**.

The jupyterlab server runs on the port 8888 and the WebUI for spark is at port 4040.

For bigquery jupyter notebook tutorial, certain steps need to be taken before running the notebook. 
1. Create a Service Account in GCP with Storage Admin and BigQuery Admin roles. Create keys for that account. Copy the contents of the keys into a file named `gcp_credentials.json` under the `credentials` folder.
2. Create a `config.yaml` in credentials folder by copying the contents from `[PLACEHOLDER]config.yaml`.
3. Put the project id in the `config.yaml` file against the PROJECT_ID key in the newly created `config.yaml` file.
3. Create a GCS bucket and store the name of the bucket against the BUCKET_NAME key in the `config.yaml` file.
4. Create a BigQuery Dataset and store the name of the dataset against the BQ_DATASET_NAME key in the `config.yaml` file.
After these steps, the notebook can be run.
