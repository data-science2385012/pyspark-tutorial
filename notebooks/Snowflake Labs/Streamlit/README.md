The command to start streamlit server: `streamlit run streamlit_app.py`
<br/>
The lab is accessible at `https://<studiolab-instance-id>.studio.<region>.sagemaker.aws/studiolab/default/jupyter/lab` and the server will be available at `https://<studiolab-instance-id>.studio.<region>.sagemaker.aws/studiolab/default/jupyter/proxy/8501/`. The slash at the end is very important.

Streamlit can be made to run in other ports as well. For example, if port 6001 needs to be used, the command and corresponding URL would be: <br/>
`streamlit run streamlit_app.py --server.port=6001` <br/>
`https://<studiolab-instance-id>.studio.<region>.sagemaker.aws/studiolab/default/jupyter/proxy/6001/`